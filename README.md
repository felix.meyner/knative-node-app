# Knative-node-app

A simple "hello world" app to get started with Knative (based on [this Knative sample](https://github.com/knative/docs/tree/master/docs/serving/samples/hello-world/helloworld-nodejs))

See the GitLab [Serverless documentation](https://docs.gitlab.com/ee/user/project/clusters/serverless/) for more information.

